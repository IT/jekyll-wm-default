# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "jekyll-wm-default"
  spec.version       = "0.2.0"
  spec.authors       = ["Phil Fenstermacher"]
  spec.email         = ["pcfens@wm.edu"]

  spec.summary       = "A simple W&M theme that can be used with jekyll for static sites"
  spec.homepage      = "https://code.wm.edu/IT/jekyll-wm-default"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", ">= 4.3.3"

  spec.add_development_dependency "bundler", "~> 2.1.4"
  spec.add_development_dependency "rake", "~> 13.2.1"
end
