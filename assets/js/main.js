(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');


function viewport() {
   var e = window, a = 'inner';
   if (!('innerWidth' in window )) {
       a = 'client';
       e = document.documentElement || document.body;
   }
   return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}

function checkTitlePosition() {
	var pos = $("#mobile_header h1").position();
	if (pos.top > 50) {
		$("#mobile_header").addClass("no-border");
	} else {
		$("#mobile_header").removeClass("no-border");
	}
}

function equalHeightColumns() {
	var bodyHeight = $("#panel").height();
	var menuHeight = $("#desktop_header").height() + $("#pgNav").height();
	if (bodyHeight < menuHeight) {
		$("#panel").height(menuHeight+35);
	}
}

var bodyWidth = viewport().width;

var slideout = null;

$(document).ready(function(){
	checkTitlePosition();
	equalHeightColumns();
	if (!$("html").hasClass('lt-ie10')) {
		slideout = new Slideout({
		    'panel': document.getElementById('panel'),
		    'menu': document.getElementById('menu'),
		    'padding': 274,
		    'tolerance': 70,
		    'side': "right"
		});
		// Toggle button
	    $('.menu_button').click(function() {
	        slideout.toggle();
	    });
	    slideout.on('beforeopen', function(){
		    $("#menu").css("display", "block");
	    });
	    slideout.on('open', function(){
	        $("#menu_mask").addClass('is-active');
	    });
	    slideout.on('close', function(){
	        $("#menu_mask").removeClass('is-active');
	    })
	
		$("#menu_mask").click(function(e){
			e.preventDefault();
			slideout.close();
			$(this).removeClass("is-active");
		});
	} else {
		$(".menu_button").click(function(){
			$("#menu").toggleClass("slideout-active");
		});
		$("#menu_mask").click(function(e){
			e.preventDefault();
			$("#menu").removeClass("slideout-active");
		});
	}
});

$(window).smartresize(function(){
	bodyWidth = viewport().width;
	if (bodyWidth >= 700) {
		if (slideout != null) {
			slideout.close();
		}
		checkTitlePosition();
		equalHeightColumns();
	} 
});
